//
//  AppDelegate.h
//  MyTest
//
//  Created by Aydar on 08.07.13.
//  Copyright (c) 2013 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

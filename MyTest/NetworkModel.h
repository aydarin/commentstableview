//
//  NetworkModel.h
//  MyTest
//
//  Created by Aydar on 08.07.13.
//  Copyright (c) 2013 AM. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NetworkModelDelegate <NSObject>

- (void) commentsLoadingComplete: (NSArray*) comments;

@end

@interface NetworkModel : NSObject

@property (nonatomic, assign) id<NetworkModelDelegate> delegate;

+(NetworkModel*)instance;
- (void) getJSONFromFile;

@end

//
//  NetworkModel.m
//  MyTest
//
//  Created by Aydar on 08.07.13.
//  Copyright (c) 2013 AM. All rights reserved.
//

#import "NetworkModel.h"

@implementation NetworkModel

+(NetworkModel*)instance {
    
#ifdef DEBUGX
    NSLog(@"%s", __FUNCTION__);
#endif
    
    static NetworkModel* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


- (void) getJSONFromFile {
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"txt"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath];
    if (JSONData) {
        id JSON = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:nil];
            
        NSArray* commentsArray = [[NSArray alloc] initWithArray:JSON];
        [_delegate commentsLoadingComplete: commentsArray];
    } else {
        NSLog(@"JSON loading error");
    };
}

@end

//
//  ViewController.h
//  MyTest
//
//  Created by Aydar on 08.07.13.
//  Copyright (c) 2013 AM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkModel.h"
#include <sys/sysctl.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NetworkModelDelegate> {
    NSArray* commentsArray;
    UITableView* commentsTable;
    NSMutableArray* sections;
}

@end

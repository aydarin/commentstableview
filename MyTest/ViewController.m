//
//  ViewController.m
//  MyTest
//
//  Created by Aydar on 08.07.13.
//  Copyright (c) 2013 AM. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [NetworkModel instance].delegate = self;
    
    commentsTable = [[UITableView alloc] initWithFrame: [self getTableFrame] style:UITableViewStyleGrouped];
    commentsTable.dataSource = self;
    commentsTable.delegate = self;
    [self.view addSubview: commentsTable];
        
    [[NetworkModel instance] getJSONFromFile];
}

- (void) commentsLoadingComplete:(NSArray *)comments {
    NSDateFormatter *dateFormatter = [self getDateFormatter];
    
    commentsArray = [comments sortedArrayUsingComparator: ^NSComparisonResult(id obj1, id obj2) {
        NSDictionary* dict1 = (NSDictionary*) obj1;
        NSDictionary* dict2 = (NSDictionary*) obj2;
        
        NSDate *date1 = [dateFormatter dateFromString:dict1[@"datetime"]];
        NSDate *date2 = [dateFormatter dateFromString:dict2[@"datetime"]];
        
        return [date2 compare:date1];
    }];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *currentComps = nil;
    sections = [[NSMutableArray alloc] init];
    NSDate* currentDate = nil;
    NSMutableArray* currentArray = nil;
    NSString* currentPost = nil;
    for (NSDictionary* comment in commentsArray) {
        if (currentDate == nil) {
            currentDate = [dateFormatter dateFromString:comment[@"datetime"]];
            currentPost = comment[@"post"];
            NSDictionary* postDict = [NSDictionary dictionaryWithObject:comment[@"post"] forKey:@"onlyPost"];
            currentComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:currentDate];
            currentArray = [[NSMutableArray alloc] init];
            [currentArray addObject:postDict];
            [currentArray addObject:comment];
        } else {
            NSDate* newDate = [dateFormatter dateFromString:comment[@"datetime"]];
            NSDateComponents *newComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate: newDate];
            if ((newComps.year == currentComps.year) && (newComps.month == currentComps.month) && (newComps.day == currentComps.day)) {
                if ([currentPost isEqualToString: comment[@"post"]]) {
                    [currentArray addObject:comment];
                } else {
                    currentPost = comment[@"post"];
                    NSDictionary* postDict = [NSDictionary dictionaryWithObject:comment[@"post"] forKey:@"onlyPost"];
                    [currentArray addObject:postDict];
                    [currentArray addObject:comment];
                }
            } else {
                [sections addObject:currentArray];
                
                currentDate = [dateFormatter dateFromString:comment[@"datetime"]];
                currentPost = comment[@"post"];
                NSDictionary* postDict = [NSDictionary dictionaryWithObject:comment[@"post"] forKey:@"onlyPost"];
                currentComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:currentDate];
                currentArray = [[NSMutableArray alloc] init];
                [currentArray addObject:postDict];
                [currentArray addObject:comment];
            }
        }
    }
    if (currentArray) [sections addObject:currentArray];

    [commentsTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (!sections) return 0;
    if (section < sections.count) {
        return ((NSArray*)sections[section]).count;
    }
    return 0;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (!sections) return 0;
    return sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName = @"";
    
    if (((NSArray*)sections[section]).count > 1) {
        NSDictionary* firstComment = ((NSArray*)sections[section])[1];
        
        NSDateFormatter* dateFormatter = [self getDateFormatter];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDate* currentDate = [dateFormatter dateFromString:firstComment[@"datetime"]];
        NSDateComponents*  currentComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:currentDate];
        
        sectionName = [NSString stringWithFormat:@"%2d.%2d.%4d", currentComps.day, currentComps.month, currentComps.year];
        sectionName = [sectionName stringByReplacingOccurrencesOfString:@" " withString:@"0" options:0 range:NSMakeRange(0, sectionName.length)];
    }
    
    return sectionName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSDictionary* comment = ((NSArray*)sections[indexPath.section])[indexPath.row];
    if (comment[@"onlyPost"]) {
        cell.textLabel.text = [NSString stringWithFormat: @"%@", comment[@"onlyPost"]];
        cell.detailTextLabel.text = @"";
    } else {
        cell.textLabel.text = [NSString stringWithFormat: @"    %@", comment[@"text"]];
        cell.detailTextLabel.text = [NSString stringWithFormat: @"     %@", comment[@"datetime"]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (CGRect) getTableFrame {
    CGRect frame = CGRectMake(0, 0, 320, 548);
    NSString* model = [self getModel];
    if ([model compare:@"iPhone5"] == NSOrderedAscending || [model rangeOfString:@"iPhone"].location == NSNotFound) {
        frame = CGRectMake(0, 0, 320, 460);
    }
    return frame;
}
- (NSString *)getModel {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
    return deviceModel;
}

- (NSDateFormatter*) getDateFormatter {
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH.mm dd.MM.yyyy"];
    return dateFormatter;
}

@end
